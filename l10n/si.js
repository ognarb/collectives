OC.L10N.register(
    "collectives",
    {
    "Cancel" : "අවලංගු",
    "Edit" : "සංස්කරණය",
    "Could not rename the page" : "පිටුව නැවත නම් කිරීමට නොහැකි විය",
    "Add a page" : "පිටුවක් එකතු කරන්න",
    "Current version" : "වත්මන් අනුවාදය",
    "No other versions available" : "වෙනත් අනුවාද නැත",
    "New Page" : "නව පිටුව"
},
"nplurals=2; plural=n > 1;");
