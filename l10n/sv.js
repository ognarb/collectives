OC.L10N.register(
    "collectives",
    {
    "Collectives" : "Kollektiv",
    "Collectives - Page content" : "Kollektiv - sidoinnehåll",
    "Collectives - Pages" : "Kollektiv - Sidor",
    "in Collective {collective}" : "i kollektiv {kollektiv}"
},
"nplurals=2; plural=n != 1;");
